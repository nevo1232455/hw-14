#include "philosophers .h"



philosophers::philosophers()
{
	for (int i = 0; i < 5; i++)
		_fork[i] = unique_lock<std::mutex>(_mu[i], std::defer_lock);
	_screen = std::unique_lock<std::mutex>(_screenMutex, std::defer_lock);
}

void philosophers::writeToScreen(int philIndex, string msg)
{
	_screen.lock();
	cout << "philosopher number " << philIndex << msg << endl;
	_screen.unlock();
}
void philosophers::eat(int philIndex)
{
	int nextInd;
	if (philIndex == 0)
		nextInd = 4;
	else
		nextInd = philIndex-1;
	_fork[philIndex].lock();
	writeToScreen(philIndex, " locked his left fork");
	_fork[nextInd].lock();
	writeToScreen(philIndex, " locked his right fork");
	_fork[philIndex].unlock();
	_fork[nextInd].unlock();
}




