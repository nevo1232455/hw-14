#pragma once
#include <iostream>
#include <mutex>
#include <string>
using namespace std;


class philosophers
{
private:
	mutex _mu[5];
	unique_lock<mutex> _fork[5], _screen;
	std::mutex _screenMutex;

public:
	philosophers();
	void writeToScreen(int philIndex, string msg);
	void eat(int philIndex);
};

