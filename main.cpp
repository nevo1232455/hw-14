#include "MessagesSender.h"
#include <thread>


void main()
{
	MessagesSender ms;
	
	// Init the threads.
	std::thread tr1(&MessagesSender::readFileForMessages, &ms);
	std::thread tr2(&MessagesSender::sendMessages, &ms);

	tr1.detach();
	tr2.detach();

	while (true) 
	{
		ms.showMenu();
	} 

}